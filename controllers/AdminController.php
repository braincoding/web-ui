<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10.07.2016
 * Time: 12:40
 */

namespace app\controllers;


use yii\web\Controller;

class AdminController extends Controller
{

        public function actionIndex()
        {
            $this->layout = '@app/views/layouts/admin.php';
            return $this->render('index');
        }
}