<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.07.2016
 * Time: 14:55
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "site".
 *
 * @property integer $id
 * @property string $name
 *
 */
class Site extends ActiveRecord
{
    const maxNameLength = 2048;
    private static $sites = array(0 => 'line.ru', 1 => 'news.ru', 2 => 'aif.ru');

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'string', 'max' => Site::maxNameLength],
            [['name'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Сайт',
        ];
    }

    public function getSites()
    {
        return self::$sites;
    }
}