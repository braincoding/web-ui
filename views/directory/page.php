<?php

/* @var $this yii\web\View */
/* @var $sites app\models\Site */
$this->title = "Сайты";
$this->params['breadcrumbs'][] = $this->title;
?>
<table class="generalpopularity table-responsive table-hover table-striped table">
	<thead>
	<tr>
		<th>Наименование</th>
	</tr>
	</thead>
	<tbody>
	<?foreach ($sites as $site){?>
		<tr>
			<td><?=$site;?></td>
		</tr>
	<?}?>
	</tbody>
</table>
<div class="btn btn-default col-lg-offset-1 col-lg-2">
	Добавить
</div>
<div class="btn btn-default col-lg-offset-1 col-lg-2">
	Редактировать
</div>
<div class="btn btn-default col-lg-offset-1 col-lg-2">
	Удалить
</div>