<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout'],
//                'rules' => [
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

//    public function actions()
//    {
//        return [
//            'error' => [
//                'class' => 'yii\web\ErrorAction',
//            ],
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],
//        ];
//    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {

        if (!Yii::$app->myUsers->isGuest()) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $post = Yii::$app->request->post();
        if (!empty($post)) {
            Yii::$app->curl->post('quickstat.cf/rest_php/v2/auth/login', 'login=' . $post['LoginForm']['login'] . "&password=" . $post['LoginForm']['password']);
            $res = Yii::$app->curl->getResponse();
            //echo"<pre>";print_r($res);echo"</pre>";
//            setcookie('token',$res['content']);
//           $_COOKIE['token']=$res['content'];
//            Yii::$app->response->cookies->add(new \yii\web\Cookie([
//                'name' => 'token',
//                'value' => $res['content'],
//            ]));
//
//            Yii::$app->response->cookies->add(new \yii\web\Cookie([
//                'name' => 'login',
//                'value' => $post['LoginForm']['login'],
//            ]));
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        //$cookies = Yii::$app->response->cookies;
       // $cookies->remove('token');
      //  $cookies->remove('login');
        Yii::$app->curl->post('quickstat.cf/rest_php/v2/auth/logout');

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionRegistration()
    {
        return $this->render('registration');
    }
}
