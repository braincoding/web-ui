<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.07.2016
 * Time: 14:56
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "personPageRank".
 *
 * @property integer $id
 * @property integer $personId
 * @property integer $pageId
 * @property integer $rank
 *
 */
class PersonPageRank extends ActiveRecord
{

    public $personId;
    public $pageId;
    public $data;
    private static $personRangeRank = [
        ['personId' => 0, 'pageId' => 0, 'rank' => 1000],
        ['personId' => 1, 'pageId' => 0, 'rank' => 1400],
        ['personId' => 2, 'pageId' => 0, 'rank' => 99],
        ['personId' => 3, 'pageId' => 0, 'rank' => 325]
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%personPageRank}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'personId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'pageId']);
    }

    public function getPersonRangeRank()
    {
        $personModel = new Person();
        foreach (self::$personRangeRank as $key => $person) {

            self::$personRangeRank[$key]['name'] = $personModel->getPerson($person['personId']);
        }
        return self::$personRangeRank;
    }
}