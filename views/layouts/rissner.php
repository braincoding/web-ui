<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
</head>
<body>
<?php $this->beginBody() ?>
<div class="col-md-2 sidebar">
    <div class="sidebar_top">
        <h1><a href="/">Quick<span>Stat</span></a></h1>
    </div>
        <div class="top-navigation">
            <div class="t-menu">MENU</div>

            <div class="t-img">
                <img src="/images/lines.png" alt=""/>
            </div>
            <div class="clearfix"></div>
        </div>
    <div class="drop-navigation">
        <div class="top-menu">
            <? echo Menu::widget([
                'items' => [
                    ['label' => 'Главная', 'url' => ['/site/index']],
                    ['label' => 'О нас', 'url' => ['/site/about']],
                    ['label' => 'Контакты', 'url' => ['/site/contact']],
                    ['label' => 'Полная статистика', 'url' => ['/statistic/full']],
                    ['label' => 'Ежедневная статистика', 'url' => ['/statistic/daily']],
                    ['label' => 'Личности', 'url' => ['/directory/person'], 'visible' => !Yii::$app->myUsers->isGuest()],
                    ['label' => 'Ключевые слова', 'url' => ['/directory/keyword'],'visible' => !Yii::$app->myUsers->isGuest()],
                    ['label' => 'Сайты', 'url' => ['/directory/page'],'visible' => !Yii::$app->myUsers->isGuest()],
                    Yii::$app->myUsers->isGuest() ? (
                    ['label' => 'Войти', 'url' => ['/site/login']]
                    ) : (
                    ['label' => 'Выйти (' . Yii::$app->request->cookies->getValue('login') . ')', 'url'=>['/site/logout'],'template'=>'<a href="{url}" data-method="post">{label}</a>']

                    )

                ],
                'options' => [
                    'id' => 'navid',
                    'class' => '',
                    'style' => 'font-size: 16px;',
                    'data' => 'menu',
                ],
                'encodeLabels' => 'false',
                'activateParents' => 'true',
                'activeCssClass' => 'active',
                'submenuTemplate' => '<ul class="top-navigation">{items}</ul>'
            ]); ?>

        </div>
        <!-- script-for-menu -->


        <div class="side-btm">
            <!--            <div class="social-icons">-->
            <!--                <ul>-->
            <!--                    <li><a href="#"><span class="fa"> </span></a></li>-->
            <!--                    <li><a href="#"><span class="tw"> </span></a></li>-->
            <!--                    <li><a href="#"><span class="g"> </span></a></li>-->
            <!--                    <li><a href="#"><span class="in"> </span></a></li>-->
            <!--                </ul>-->
            <!--                <div class="clearfix"></div>-->
            <!--            </div>-->
            <div class="copyright">
                <p> © 2016 Brain Coding. All rights reserved</p>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-3 main">
    <div class="side-content">
        <div class="banner">


            <div class="slider">
                <div class="callbacks_container">
                    <ul class="rslides" id="slider">
                        <li>
                            <h2>Полная статистика</h2>
                            <h3>Выбери ресурс.</h3>
                        </li>
                        <li>
                            <h2>Ежедневная статистика</h2>
                            <h3>Детально рассмотри все.</h3>
                        </li>
                        <li>
                            <h2>Больше возможностей</h2>
                            <h3>Регистрируйся и получай больше функций.</h3>
                        </li>
                    </ul>
                </div>
            </div>
            <!----->
        </div>
    </div>
    <div class="clearfix"></div>
    <?= Breadcrumbs::widget(['homeLink' => ['label' => 'Главная', 'url' => ['/site/index'],
        'class' => 'home',],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <div class="main-content">
        <?= $content ?>
    </div>

    <div class="footer">
        <div class="footer-grids">
            <div class="col-md-6 ftr-grid1">
                <h4>О нас</h4>
                <p>Молодая. Маленькая. Перспективная. Мы - команда.</p>

            </div>
            <!--            <div class="col-md-6 news-ltr">-->
            <!--                <h4>Newsletter</h4>-->
            <!--                <p>Aenean sagittis est eget elit pulvinar cursus. Lorem ipsum dolor sit amet, consectetur adipiscing-->
            <!--                    elit. Phasellus non purus at risus consequat finibus.</p>-->
            <!--                <form>-->
            <!--                    <input type="text" class="text" value="Enter Email" onfocus="this.value = '';"-->
            <!--                           onblur="if (this.value == '') {this.value = 'Enter Email';}">-->
            <!--                    <input type="submit" value="Subscribe">-->
            <!--                    <div class="clearfix"></div>-->
            <!--                </form>-->
            <!--            </div>-->
            <div class="clearfix"></div>
        </div>
    </div>

</div>
<div class="clearfix"></div>
<?php $this->endBody() ?>
<script>
    $(".top-navigation").click(function () {
        $(".drop-navigation").slideToggle(300, function () {
            // Animation complete.
        });
    });
</script>
<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            nav: false,
            speed: 500,
            namespace: "callbacks",
            pager: true
        });
    });
</script>
</body>

</html>
<?php $this->endPage() ?>
