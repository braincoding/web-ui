<?php
/* @var $this yii\web\View */
/* @var $persons app\models\Person */
$this->title = "Личности";
$this->params['breadcrumbs'][] = $this->title;
//echo"<pre>";print_r($persons);echo"</pre>";
//die();
?>
<table class="generalpopularity table-responsive table-hover table-striped table">
	<thead>
	<tr>
		<th>Наименование</th>
	</tr>
	</thead>
	<tbody>
	<?foreach ($persons as $person){?>
		<tr>
			<td><?=$person;?></td>
		</tr>
	<?}?>
	</tbody>
</table>
<div class="btn btn-default col-lg-offset-1 col-lg-2">
	Добавить
</div>
<div class="btn btn-default col-lg-offset-1 col-lg-2">
	Редактировать
</div>
<div class="btn btn-default col-lg-offset-1 col-lg-2">
	Удалить
</div>