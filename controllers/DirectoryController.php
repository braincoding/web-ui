<?php

namespace app\controllers;

use Yii;
use app\models\Keyword;
use app\models\Person;
use app\models\Site;

class DirectoryController extends \yii\web\Controller
{
    public function actionPerson()
    {
        Yii::$app->curl->get('quickstat.cf/rest_php/v2/stat/common?site_id=1');
        $res = Yii::$app->curl->getResponse();
        return $this->render('person',['persons'=>$res]);
    }

    public function actionKeyword()
    {
        $person=new Person();
        $keyword=new Keyword();
        return $this->render('keyword',['persons'=>$person->getAllPersons(),'keywords'=>$keyword->getKeywords(0)]);
    }

    public function actionPage()
    {
        $site=new Site();
        //$_COOKIE["token"] = Yii::$app->request->cookies->getValue('token');
        Yii::$app->curl->get('quickstat.cf/rest_php/v2/stat/sites');
        $res = Yii::$app->curl->getResponse();
        echo"<pre>";print_r($res);echo"</pre>";
        die();
        //return $this->render('person',['persons'=>$res]);
        return $this->render('page',['sites'=>$res['content']]);
    }

}
