<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $persons app\models\Person */
/* @var $keywords app\models\Keyword */
$this->title = "Ключевые слова";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="top-title-statistic">
    <form class="form-inline" role="form">
        <div class="form-group">
            <?= Html::dropDownList('person', null, $persons, ['class' => 'form-control']); ?>
        </div>
        <input class="btn btn-default" type="button" value="Применить">
    </form>
</div>

<table class="generalpopularity table-responsive table-hover table-striped table">
    <thead>
    <tr>
        <th>Наименование</th>
    </tr>
    </thead>
    <tbody>
    <?foreach ($keywords as $keyword){?>
    <tr>
        <td><?=$keyword['name'];?></td>
    </tr>
    <?}?>
    </tbody>
</table>
<div class="btn btn-default col-lg-offset-1 col-lg-2">
    Добавить
</div>
<div class="btn btn-default col-lg-offset-1 col-lg-2">
    Редактировать
</div>
<div class="btn btn-default col-lg-offset-1 col-lg-2">
    Удалить
</div>
      