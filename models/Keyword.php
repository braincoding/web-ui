<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.07.2016
 * Time: 14:55
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "keyword".
 *
 * @property integer $id
 * @property string $name
 * @property integer $personId
 *
 * @property Person $person
 */
class Keyword extends ActiveRecord
{
    const maxNameLength=2048;
    public $data;
    private static $keywords = [
        ['personId' => 0, 'name' => 'Путину'],
        ['personId' => 0, 'name' => 'Путиным'],
        ['personId' => 0, 'name' => 'О Путине'],
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%keyword}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['personId','id'], 'integer'],
            [['name'], 'string', 'max' => Keyword::maxNameLength],
            [['name'],'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Наименование',
            'personId'=>'Личность'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['personId' => 'id']);
    }
    
    public function getKeywords($personId){
        foreach (self::$keywords AS $keyword){
            if($keyword['personId']==$personId){
                $this->data[]=$keyword;
            }
        }
        return $this->data;
    }
}