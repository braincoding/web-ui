<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.07.2016
 * Time: 21:02
 */
$this->title = "Вход на сайт";
?>
<div class="sign-in-up">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <br>
            <!-- Nav tabs -->
            <div class="text-center">
                <div class="btn-group">
                   <a href="#new" data-toggle="tab" class="btn btn-xs btn-info"> <i class="glyphicon glyphicon-plus"></i> Регистрация</span></a>
                   <a href="#user"  data-toggle="tab" class="btn btn-xs btn-warning"> <i class="glyphicon glyphicon-user"></i> Есть аккаунт</span></a>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="new">
                    <br>
                    <fieldset>
                        <div class="form-group">
                            <div class="right-inner-addon">
                                <i class="fa fa-envelope"></i>
                                <input class="form-control input-lg" placeholder="Логин" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="right-inner-addon">
                                <i class="fa fa-key"></i>
                                <input class="form-control input-lg" placeholder="Пароль" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="right-inner-addon">
                                <i class="fa fa-key"></i>
                                <input class="form-control input-lg" placeholder="Повтор пароля" id="" type="password">
                            </div>
                        </div>
                    </fieldset>
                    <div class="tab-content">
                        <div class="submit-btn" id="pp">
                            <input type="submit" value="Отправить" />
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="user">
                    <br>
                    <fieldset>
                        <div class="form-group">
                            <div class="right-inner-addon">
                                <i class="fa fa-envelope"></i>
                                <input class="form-control input-lg" placeholder="Email Address" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="right-inner-addon">
                                <i class="fa fa-key"></i>
                                <input class="form-control input-lg" placeholder="Password" type="password">
                            </div>
                        </div>
                    </fieldset>
                    <br>
                    <div class="submit-btn">
                        <input type="submit" value="Отправить" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
