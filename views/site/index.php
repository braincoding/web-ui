<?php

/* @var $this yii\web\View */

$this->title = 'QuickStat';
?>
<div class="welcome">
    <div class="welcm-grids">
        <div class="welcm-info">
            <h3>Добро пожаловать!</h3>
            <h4>на сайт аналитики популярности известных людей в новостных ресурсах</h4>
            <p>На данном сайте Вы можете узнать насколько часто нужный Вам человек упоминается в новостных ресурах
                задав ключевые слова, по каким его можно определить</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!----->
<div class="services">
    <div class="service-sec">
        <h3>Услуги</h3>
        <div class="service_grids">
            <div class="col-md-4 service_grid text-center">
                <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
                <h6><a href="/statistic/full">Полная статистика</a></h6>
                <p>Выберите ресурс и мы Вам покажем полную статистику по нему</p>
            </div>
            <div class="col-md-4 service_grid text-center">
                <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                <h6><a href="/statistic/daily">Ежедневная статистика</a></h6>
                <p>Выберите ресурс, личность, укажите диапазон дат и мы Вам покажем детальную статистику за период.</p>
            </div>
            <div class="col-md-4 service_grid text-center">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <h6><a href="/site/registration">Регистрация</a></h6>
                <p>Создайте свой аккаунт и получите возможность вносить свои ресурсы и личности.</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!---->
<!--    <!---->
<!--    <div class="news">-->
<!--        <div class="news-sec">-->
<!--            <h3>Latest News</h3>-->
<!--            <div class="news-top">-->
<!--                <div class="col-md-6 news-1">-->
<!--                    <h5>Vivamus ante lorem bibend</h5>-->
<!--                    <h6>10:00 to 11:00 | Jan 09,2014</h6>-->
<!--                    <p>Praesent tincidunt quam purus, quis tincidunt magna iaculis vitae. In fermentum dui lectus, eu-->
<!--                        cursus tellus pulvinar sed. Duis a eros at sapien vestibulum.</p>-->
<!--                    <a class="read" href="about.html">More..</a>-->
<!--                </div>-->
<!--                <div class="col-md-6 news-1">-->
<!--                    <h5>Vivamus ante lorem bibend</h5>-->
<!--                    <h6>10:00 to 11:00 | Jan 09,2014</h6>-->
<!--                    <p>Praesent tincidunt quam purus, quis tincidunt magna iaculis vitae. In fermentum dui lectus, eu-->
<!--                        cursus tellus pulvinar sed. Duis a eros at sapien vestibulum.</p>-->
<!--                    <a class="read" href="about.html">More..</a>-->
<!--                </div>-->
<!--                <div class="clearfix"></div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="clearfix"></div>-->
<!--    </div>-->
<!--    <!---->
<!-- footer -->
