<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.07.2016
 * Time: 14:55
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "person".
 *
 * @property integer $id
 * @property string $name
 *
 */
class Person extends ActiveRecord
{

    const maxNameLength = 2048;
    private static $person = [
        0 => 'Путин',
        1 => 'Медведев',
        2 => 'Навальный',
        3 => 'Фриске'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%person}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'string', 'max' => Person::maxNameLength],
            [['name'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Личность',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords()
    {
        return $this->hasMany(Keyword::className(), ['id' => 'personId']);
    }

    public function getPerson($personId)
    {
        return self::$person[$personId];
    }

    public function getAllPersons()
    {
        return self::$person;
    }
}