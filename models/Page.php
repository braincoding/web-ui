<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.07.2016
 * Time: 14:56
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $url
 * @property integer $siteId
 * @property string $foundDateTime
 * @property string $lastScanDate
 *
 */
class Page extends ActiveRecord
{
    const maxNameLength = 2048;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','siteId'], 'integer'],
            [['url'], 'string', 'max' => Site::maxNameLength],
            [['foundDateTime','lastScanDate'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => 'Ссылка',
            'siteId' => 'Сайт',
        ];
    }
}