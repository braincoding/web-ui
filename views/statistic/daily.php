<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.07.2016
 * Time: 16:45
 */
$this->title = "Ежедневная статистика";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="top-title-statistic">
    <form class="form-horizontal" role="form">
        <div class="form-group">
            <label for="site" class="col-sm-2 control-label">Сайт</label>
            <div class="col-sm-5">
                <select class="form-control" name="site" id="site">
                    <option>line.ru</option>
                    <option>news.ru</option>
                    <option>aif.ru</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="person" class="col-sm-2 control-label">Личность</label>
            <div class="col-sm-5">
                <select class="form-control" name="person" id="person">
                    <option>Медведев</option>
                    <option>Путин</option>
                    <option>Навальный</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6">
                <label for="dateStart" class="col-sm-4 control-label">Период с:</label>
                <div class="col-sm-8">
                    <input type="date" class="form-control" id="dateStart" placeholder="">
                </div>
            </div>
            <div class="col-sm-6">
                <label for="dateEnd" class="col-sm-1 control-label">по:</label>
                <div class="col-sm-8">
                    <input type="date" class="form-control" id="dateEnd" placeholder="">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Применить</button>
            </div>
        </div>
    </form>
</div>
<table class="table table-bordered table-condensed table-striped table-responsive table-hover">
    <thead>
    <tr>
        <th>Дата</th>
        <th>Количество новых страниц</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>15.06.2015</td>
        <td>2</td>
    </tr>
    <tr>
        <td>16.06.2015</td>
        <td>1</td>
    </tr>
    <tr>
        <td>17.06.2015</td>
        <td>4</td>
    </tr>
    <tr>
        <td>18.06.2015</td>
        <td>0</td>
    </tr>
    <tr>
        <td>19.06.2015</td>
        <td>1</td>
    </tr>
    <tr>
        <td>20.06.2015</td>
        <td>2</td>
    </tr>
    <tr>
        <td colspan="2">
            <strong>Всего за период: 10</strong>
        </td>
    </tr>
    </tbody>
</table>
