<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'QuickStat',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Главная', 'url' => ['/site/index']],
            ['label' => 'О нас', 'url' => ['/site/about']],
            ['label' => 'Контакты', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
            ['label' => 'Войти', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Выйти (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <div class="left-menu">
            <? echo Menu::widget([
                'items' => [
                    ['label' => 'Полная статистика', 'url' => ['/statistic/full']],
                    ['label' => 'Ежедневная статистика', 'url' => ['/statistic/daily']],
                    !Yii::$app->user->isGuest ? (
                    ['label' => 'Справочники', 'url' => ['#'],
                        'items' => [
                            ['label' => 'Личности', 'url' => ['/directory/person']],
                            ['label' => 'Ключевые слова', 'url' => ['/directory/keyword']],
                            ['label' => 'Сайты', 'url' => ['/directory/page']],
                        ],
                        'options' => ['class' => 'dropdown'],
                        'activateParents' => 'true',
                        'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown">{label}<b class="caret"></b></a>'
                    ]
                    ) : (""),
                ],
                'options' => [
                    'id' => 'navid',
                    'class' => 'nav nav-pills nav-stacked',
                    'style' => 'font-size: 16px;',
                    'data' => 'menu',
                ],
                'encodeLabels' => 'false',
                'activateParents' => 'true',
                'activeCssClass' => 'active',
                'submenuTemplate' => '<ul class="dropdown-menu">{items}</ul>'
            ]); ?>
        </div>
        <div class="center-block">
            <?= Breadcrumbs::widget(['homeLink' => ['label' => 'Главная', 'url' => ['/site/index'],
                'class' => 'home',],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Brain Coding <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
