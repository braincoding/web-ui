<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="container container-body">
        <div class="row">
            <header>
                <div class="header-content">
                    <div class="header-name">
                        Brain-coding
                    </div>
                    <div class="header-login btn btn-link">
                        <?= Yii::$app->user->isGuest ? (
                        Html::a("Войти",  '/site/login')
                        ) : (Html::a('Выйти (' . Yii::$app->user->identity->username . ')', '/site/logout',['data-method'=>'post'])); ?>
                    </div>
                    <?php
                    NavBar::begin([
                        'brandLabel' => false,
                        'options' => [
                            'class' => 'navbar navbar-default header-menu',
                        ],
                    ]); ?>
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse">
                            <? echo Nav::widget([
                                'options' => ['class' => 'nav navbar-nav'],
                                'items' => [
                                    ['label' => 'Главная', 'url' => ['/site/index']],
                                    ['label' => 'О нас', 'url' => ['/site/about']],
                                    ['label' => 'Контакты', 'url' => ['/site/contact']],

                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="shadow-left col-lg-4 col-xs-4 col-sm-4 col-md-4"></div>
                        <div
                            class="shadow-right col-lg-4 col-xs-4 col-sm-4 col-md-4 col-lg-offset-4 col-xs-offset-4 col-sm-offset-4 col-md-offset-4"></div>
                    </div>
                    <? NavBar::end();
                    ?>

                </div>
            </header>
            <div class="index-content">
                <div class="content-bottom">
                    <div class="left-menu">
                        <? echo Menu::widget([
                            'items' => [
                                ['label' => 'Полная статистика', 'url' => ['/statistic/full']],
                                ['label' => 'Ежедневная статистика', 'url' => ['/statistic/daily']],
                                !Yii::$app->user->isGuest ? (
                                ['label' => 'Справочники', 'url' => ['#'],
                                    'items' => [
                                        ['label' => 'Личности', 'url' => ['/directory/person']],
                                        ['label' => 'Ключевые слова', 'url' => ['/directory/keyword']],
                                        ['label' => 'Сайты', 'url' => ['/directory/page']],
                                    ],
                                    'options' => ['class' => 'dropdown'],
                                    'activateParents' => 'true',
                                    'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown">{label}<b class="caret"></b></a>'
                                ]
                                ) : (""),
                            ],
                            'options' => [
                                'id' => 'navid',
                                'class' => 'nav nav-pills nav-stacked',
                                'style' => 'font-size: 16px;',
                                'data' => 'menu',
                            ],
                            'encodeLabels' => 'false',
                            'activateParents' => 'true',
                            'activeCssClass' => 'active',
                            'submenuTemplate' => '<ul class="dropdown-menu">{items}</ul>'
                        ]); ?>
                    </div>
                    <div class="center-block">
                        <?= Breadcrumbs::widget(['homeLink' => ['label' => 'Главная', 'url' => ['/site/index'],
                            'class' => 'home',],
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                        <?= $content ?>
                    </div>
                </div>
            </div>
            <footer>
                &copy; Brain Coding <?= date('Y') ?>
            </footer>
        </div>
    </div>

    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>