<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.07.2016
 * Time: 11:40
 */

namespace app\components;
class Curl
{
    private $curl;
    private $response;

    public function __construct()
    {
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_HEADER, true);
        curl_setopt($this->curl, CURLOPT_VERBOSE, true);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
    }

    private function prepareCookie()
    {
        if (count($_COOKIE) > 0) {
            $cookie = array();

            foreach ($_COOKIE as $key => $value) {
                $cookie[] = "$key=$value";
            }

            $cookie = implode(';', $cookie);
            curl_setopt($this->curl, CURLOPT_COOKIE, $cookie);
        }
    }

    private function processHeaders($headers)
    {
        foreach ($headers as $value) {
            $header = explode(': ', $value, 2);
            if ($header[0] == 'Set-Cookie') {
                header($value);
            }
        }

    }

    public function getResponse()
    {
        return $this->response;
    }

    private function processResponse($response)
    {
        $response = explode("\r\n\r\n", $response);
        $this->response['headers'] = explode("\r\n", $response[0]);

        $responseStatus = explode(' ', $this->response['headers'][0], 3);
        $this->response['info'] = $responseStatus[2];
        $this->response['code'] = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        $this->response['contentType'] = curl_getinfo($this->curl, CURLINFO_CONTENT_TYPE);
        if (isset($response[1])) {
            $this->response['content'] = $response[1];
        } else {
            $this->response['content'] = '';
        }

        $this->processHeaders($this->response['headers']);
    }

    public function get($url, $params = NULL)
    {
        if (isset($params)) {
            $url = $url . '?' . $params;
        }
        curl_setopt($this->curl, CURLOPT_POST, false);
        $this->send($url);
    }

    public function post($url, $params = NULL)
    {
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params);
        $this->send($url);
    }

    public function delete($url, $params = NULL)
    {
        if (isset($params)) {
            $url = $url . '?' . $params;
        }
        
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        $this->send($url);
    }

    private function send($url)
    {
        $this->prepareCookie();
        curl_setopt($this->curl, CURLOPT_URL, $url);
        $res = curl_exec($this->curl);
        $this->processResponse($res);
    }
}