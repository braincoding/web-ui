<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.07.2016
 * Time: 17:01
 */
namespace app\controllers;

use yii\rest\ActiveController;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';
}