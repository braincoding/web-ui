<?php

namespace app\controllers;

use Yii;
use app\models\PersonPageRank;
use app\models\Site;

class StatisticController extends \yii\web\Controller
{
    public function actionFull()
    {
        $statistic = new PersonPageRank();
        $sites = new Site();
        Yii::$app->curl->get('quickstat.cf/rest_php/v2/stat/common?site_id=1');
        $res = Yii::$app->curl->getResponse();
        //echo"<pre>";print_r($res);echo"</pre>";die();
        return $this->render('full', ['statistic' => $statistic->getPersonRangeRank(), 'sites' => $sites->getSites()]);
    }

    public function actionDaily()
    {
        return $this->render('daily');
    }

}
