<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.07.2016
 * Time: 16:45
 */
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $statistic app\models\PersonPageRank */
/* @var $sites app\models\Site */
$this->title = "Полная статистика";
//$personRangeRank=new \app\models\PersonPageRank();
$this->params['breadcrumbs'][] = $this->title;
//echo "<pre>";
//print_r($statistic);
//echo "</pre>";
//echo "<pre>";
//print_r(ArrayHelper::toArray($statistic, ['app\models\PersonPageRank' => ['name', 'y' => 'rank']]));
//echo "</pre>";
?>

<div class="top-title-statistic">
    <form class="form-inline" role="form">
        <div class="form-group">
            <?= Html::dropDownList('site', null, $sites, ['class' => 'form-control']); ?>
        </div>
        <input class="btn btn-default" type="button" value="Применить">
    </form>
</div>
<table class="table table-bordered table-condensed table-striped table-responsive table-hover">
    <thead>
    <tr>
        <th>Имя</th>
        <th>Количество упоминаний</th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($statistic AS $personRangeRank) { ?>
        <tr>
            <td><?= $personRangeRank['name']; ?></td>
            <td><?= $personRangeRank['rank']; ?></td>
        </tr>
    <? } ?>
    </tbody>
</table>
<div>
<? echo Highcharts::widget([
    'options' => [
        'chart' => ['type' => 'column'],
        'title' => ['text' => 'Общая статистика'],
        'xAxis' => [
            'type' => 'category'
        ],
        'yAxis' => [
            'title' => ['text' => 'Кол-во упоминаний']
        ],
        'legend' => ['enable' => false],
        'series' => [

            [
                'name' => 'Личность',
                'colorByPoint' => true,
                'data' => [[
                    'name'=>'Путин',
                    'y'=>1000
                ],[
                    'name'=>'Медведев',
                    'y'=>1400
                ],[
                    'name'=>'Навальный',
                    'y'=>99
                ],[
                    'name'=>'Фриске',
                    'y'=>325
                ]]

            ]

        ]
    ]]);
?>
</div>
